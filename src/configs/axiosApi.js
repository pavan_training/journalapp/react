import axios from "axios";

var axiosApi = axios.create({
  baseURL: "http://localhost:8085",
});

const apis = {
  LOGIN: "/login",
  SIGNUP: "/addUser",

  ADD_STORY: "/addStory",
  UPDATE_STORY: "/updateStory",
  GET_ALL_STORIES: "/getAllStories",
  GENERATE_OTP: "/generateOtp",
  GET_STORIES_BETWEEN_DATE_RANGE: "/getStoriesBetweenDateRange",
};

export { axiosApi, apis };
