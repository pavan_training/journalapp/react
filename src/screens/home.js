import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { useForm } from "react-hook-form";
import StoryCard from "../components/storyCard";
import { axiosApi, apis } from "../configs/axiosApi";
import DateRangePicker from "@wojtekmaj/react-daterange-picker";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

function Home(props) {
  const classes = useStyles();
  const { register, handleSubmit, errors } = useForm();
  const [storyList, setStoryList] = useState([]);
  const [dateRange, setDateRange] = React.useState("");
  let history = useHistory();

  useEffect(() => {
    getStoriesFromApi();
  }, []);

  function submitData(data) {
    console.log(data);

    console.log(errors);
  }

  function getStoriesFromApi() {
    var requesPparams = {
      email: localStorage.getItem("email"),
    };

    axiosApi
      .get(apis.GET_ALL_STORIES, { params: requesPparams })
      .then((res) => {
        console.log(res.data);
        setStoryList(res.data.payload);
      });
  }

  const handleDateChange = (e) => {
    setDateRange(e);

    // call api to get data between fromDate and toDate
    axiosApi
      .post(apis.GET_STORIES_BETWEEN_DATE_RANGE, {
        fromDate: new Date(e[0]),
        toDate: new Date(e[1]),
        email: localStorage.getItem("email"),
      })
      .then((res) => {
        setStoryList(res.data.payload);
      })
      .catch((err) => {})
      .finally(() => {});
  };

  const handleStoryOnclick = (val) => {
    history.push("/viewStory", val);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          flex: 1,
          justifyContent: "flex-end",
          marginBottom: 24,
        }}
      >
        <DateRangePicker onChange={handleDateChange} value={dateRange} />
      </div>

      {storyList.map((val, i) => {
        return (
          <div onClick={(e) => handleStoryOnclick(val)}>
            <StoryCard
              description={val.storyContent}
              date={new Date(val.createDate)}
            ></StoryCard>
          </div>
        );
      })}

      {/* <StoryCard
        description="This is my second description"
        date={new Date("04-13-2019")}
      ></StoryCard> */}
    </div>
  );
}

export default Home;
