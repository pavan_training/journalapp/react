import React, { useState, useEffect } from "react";
import { Editor } from "react-draft-wysiwyg";
import {
  EditorState,
  convertToRaw,
  convertFromRaw,
  convertFromHTML,
  ContentState,
} from "draft-js";
import draftToHtml from "draftjs-to-html";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./addStory.css";
import { Button } from "@material-ui/core";
import { axiosApi, apis } from "../configs/axiosApi";
import { useSnackbar } from "notistack";
import { useLocation, useHistory } from "react-router-dom";

function AddStory(props) {
  const [storyText, setStoryText] = useState(EditorState.createEmpty());
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();
  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    // Updates content in editor
    if (location.state) {
      const blocksFromHTML = convertFromHTML(location.state.storyContent);
      const content = ContentState.createFromBlockArray(blocksFromHTML);
      const editorState = EditorState.createWithContent(content);
      setStoryText(editorState);
    }
  }, []);

  const addStory = () => {
    var addedText = draftToHtml(convertToRaw(storyText.getCurrentContent()));
    console.log(addedText);

    // Todo: insert data into database call addStory api
    var params = {
      storyContent: addedText,
      userEmail: localStorage.getItem("email"),
    };

    axiosApi
      .post(apis.ADD_STORY, params)
      .then((res) => {
        console.log(res.data);
        enqueueSnackbar(res.data.message, {
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        });
      })
      .catch((e) => {})
      .finally(() => {});
  };

  const onStoryTextChange = (editorState) => {
    setStoryText(editorState);
  };

  const updateStory = () => {
    var oldData = location.state;
    var textFromEditor = draftToHtml(
      convertToRaw(storyText.getCurrentContent())
    );
    oldData.storyContent = textFromEditor;

    axiosApi
      .put(apis.UPDATE_STORY, oldData)
      .then((res) => {
        console.log(res.data);
        history.push("/home");
        enqueueSnackbar(res.data.message, {
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        });
      })
      .catch((e) => {})
      .finally(() => {});
  };

  return (
    //  <Editor

    // wrapperClassName="wrapper-class"
    // editorClassName="editor-class"/>
    <div>
      <div style={{ height: 400 }}>
        <Editor
          editorState={storyText}
          onEditorStateChange={onStoryTextChange}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
        />
      </div>
      <div style={{ textAlign: "right" }}>
        {location.state ? (
          <Button onClick={updateStory} variant="contained" color="primary">
            Update Story
          </Button>
        ) : (
          <Button onClick={addStory} variant="contained" color="primary">
            Add Story
          </Button>
        )}
      </div>

      <div>
        {/* <div className="addImage" style={{display:"flex",alignItems:"center",justifyContent:"center"}}>
                    Add
                </div> */}
      </div>
    </div>
  );
}

export default AddStory;
