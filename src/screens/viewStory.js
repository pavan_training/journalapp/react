import React from "react";
import { Paper, Button } from "@material-ui/core";
import { useLocation, useHistory } from "react-router-dom";
import renderHTML from "react-render-html";

function ViewStory(props) {
  const location = useLocation();
  const history = useHistory();
  const data = location.state;

  var tempDate = new Date(data.createDate);
  var time = getTimeAMPM(tempDate);
  var date = tempDate.getDate();
  var month = getMonthString(tempDate.getMonth());
  var year = tempDate.getFullYear();

  function getMonthString(num) {
    var months = {
      0: "Jan",
      1: "Feb",
      2: "Mar",
      3: "Apr",
      4: "May",
      5: "Jun",
      6: "Jul",
      7: "Aug",
      8: "Sep",
      9: "Oct",
      10: "Nov",
      11: "Dec",
    };

    return months[num];
  }
  function getTimeAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    return strTime;
  }

  const handleEditClick = () => {
    history.push("/addStory", data);
  };

  return (
    <div>
      <Paper style={{ padding: 24 }}>
        <div
          id="date"
          style={{ display: "flex", flex: 1, justifyContent: "space-between" }}
        >
          <span style={{ fontSize: 24 }}>
            {date}-{month}-{year}
          </span>
          <div>
            <Button onClick={handleEditClick}> Edit</Button>
            <Button onClick={() => history.push("/home")}> back</Button>
          </div>
        </div>
        <div id="content" style={{ marginTop: 16, fontSize: 18 }}>
          {renderHTML(data.storyContent)}
        </div>
      </Paper>
    </div>
  );
}

export default ViewStory;
