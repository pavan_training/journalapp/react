import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useHistory, useLocation } from "react-router-dom";
import { useSnackbar } from "notistack";
import { axiosApi, apis } from "../../configs/axiosApi";
import Alert from "@material-ui/lab/Alert";
import {
  Paper,
  TextField,
  makeStyles,
  FormControl,
  Button,
  Typography,
} from "@material-ui/core";

function ResetPassword(props) {
  const location = useLocation();
  const history = useHistory();

  const [formData, setFormData] = useState({
    otp: "",
    new_password: "",
    re_password: "",
  });

  const [customAlert, setCustomAlert] = useState({
    severity: "error",
    errorMsg: null,
  });

  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  useEffect(() => {
    // otp
    // formData.otp = location.state.otp;
    var temp = { ...formData };
    temp.otp = location.state ? location.state.otp : "";
    setFormData(temp);
  }, []);

  const handleChange = (event) => {
    // console.log(event.target.name+" : "+event.target.value);
    var key = event.target.name;
    var val = event.target.value;
    setFormData({ ...formData, [key]: val });
  };

  const onUpdateClick = () => {
    // validation
    if (formData.new_password !== formData.re_password) {
      setCustomAlert({
        severity: "error",
        errorMsg: "New password & re-enter is not matching",
      });

      enqueueSnackbar("New password & re-enter is not matching", {
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      });
      return;
    }

    axiosApi
      .post("/resetPassword", formData)
      .then((res) => {
        var status = "error";

        if (res.data.status === "success") {
          status = "success";
          history.push("/resetPassword", null);
          var temp = { ...formData };
          temp.otp = "";
          setFormData(temp);
        }

        enqueueSnackbar(res.data.message, {
          variant: status,
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        });
      })
      .catch((err) => {})
      .finally(() => []);

    console.log(formData);
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        backgroundColor: "#55efc4",
      }}
    >
      <div className="loginForm" style={{ margin: 32 }}>
        {customAlert != undefined && customAlert.errorMsg !== null ? (
          <Alert severity={customAlert.severity}>{customAlert.errorMsg}</Alert>
        ) : null}

        <Paper style={{ width: 300, padding: 32 }} elevation={3}>
          <form noValidate autoComplete="off">
            <Typography variant="h5" style={{ marginBottom: 12 }}>
              Forgot Password
            </Typography>

            <TextField
              name="otp"
              onChange={handleChange}
              id="standard-basic"
              input
              type="password"
              label="Enter OTP"
              fullWidth
              value={formData.otp}
            />

            <TextField
              name="new_password"
              onChange={handleChange}
              id="standard-basic"
              input
              type="password"
              label="New Password"
              value={formData.new_password}
              fullWidth
            />

            <TextField
              name="re_password"
              onChange={handleChange}
              id="standard-basic"
              input
              type="password"
              label="Re-enter Password"
              value={formData.re_password}
              fullWidth
            />

            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: 12,
              }}
            >
              <span style={{ fontSize: 12, paddingTop: 10 }}>
                <Link to="/Login">Login?..</Link>
              </span>
              <Button
                variant="contained"
                color="primary"
                onClick={onUpdateClick}
              >
                Update
              </Button>
            </div>
            <div style={{ marginTop: 16 }}>
              <span style={{ fontSize: 12 }}>
                <Link to="signup">
                  Not registered yet? please click here to sign up
                </Link>
              </span>
            </div>
          </form>
        </Paper>
      </div>
    </div>
  );
}

export default ResetPassword;
