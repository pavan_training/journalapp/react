import React, { useState } from "react";
import {
  Paper,
  TextField,
  makeStyles,
  FormControl,
  Button,
  Typography,
} from "@material-ui/core";
import "./login.css";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useSnackbar } from "notistack";
import { axiosApi, apis } from "../../configs/axiosApi";

const useStyles = makeStyles((theme) => ({}));

function Login(props) {
  const classes = useStyles();
  let history = useHistory();
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const [userDetails, setUserDetails] = useState({});

  const handleChange = (event) => {
    // console.log(event.target.name+" : "+event.target.value);
    var key = event.target.name;
    var val = event.target.value;

    setUserDetails({ ...userDetails, [key]: val });
  };

  function onLoginClick() {
    // loading start
    axiosApi
      .post(apis.LOGIN, userDetails)
      .then(function (response) {
        var status = "";
        if (response.data.status === "success") {
          status = "success";

          var user = response.data.payload;
          localStorage.setItem(
            "username",
            user.firstname + " " + user.lastname
          );
          localStorage.setItem("email", user.email);

          props.setAuthorized(true);
        } else {
          status = "error";
        }

        enqueueSnackbar(response.data.message, {
          variant: status,
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        });
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {});
  }

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        backgroundColor: "#55efc4",
      }}
    >
      <div className="loginForm">
        <Paper style={{ width: 300, padding: 32, margin: 32 }} elevation={3}>
          <form className={classes.root} noValidate autoComplete="off">
            <Typography variant="h5" style={{ marginBottom: 12 }}>
              {" "}
              Login{" "}
            </Typography>

            <TextField
              name="userName"
              onChange={handleChange}
              id="standard-basic"
              label="Username / Email"
              fullWidth
            />

            <TextField
              name="password"
              onChange={handleChange}
              id="standard-basic"
              input
              type="password"
              label="Password"
              fullWidth
            />

            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                marginTop: 12,
              }}
            >
              <span style={{ fontSize: 12, paddingTop: 10 }}>
                <Link to="/forgotPassword">Forget password?..</Link>
              </span>
              <Button
                variant="contained"
                color="primary"
                onClick={(e) => onLoginClick()}
              >
                {" "}
                Login{" "}
              </Button>
            </div>
            <div style={{ marginTop: 16 }}>
              <span style={{ fontSize: 12 }}>
                <Link to="signup">
                  Not registered yet? please click here to sign up
                </Link>
              </span>
            </div>
          </form>
        </Paper>
      </div>
    </div>
  );
}
export default Login;
