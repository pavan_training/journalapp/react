import React, { useState } from "react";
import {
  Paper,
  TextField,
  makeStyles,
  FormControl,
  Button,
  Typography,
} from "@material-ui/core";
import "./login.css";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useSnackbar } from "notistack";
import { axiosApi, apis } from "../../configs/axiosApi";

function ForgotPassword(props) {
  const [formData, setFormData] = useState({});
  const history = useHistory();

  const handleChange = (event) => {
    // console.log(event.target.name+" : "+event.target.value);
    var key = event.target.name;
    var val = event.target.value;
    setFormData({ ...formData, [key]: val });
  };

  const generateOtp = () => {
    // call generate api

    axiosApi
      .get(apis.GENERATE_OTP, {
        params: {
          email: formData.email,
        },
      })
      .then((res) => {
        // Move to reset password screen
        history.push("/resetPassword", res.data.payload);
      })
      .catch((err) => {})
      .finally(() => {});
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        backgroundColor: "#55efc4",
      }}
    >
      <div className="loginForm">
        <Paper style={{ width: 300, padding: 26, margin: 32 }} elevation={3}>
          <form noValidate autoComplete="off">
            <Typography variant="h5" style={{ marginBottom: 12 }}>
              Forgot Password
            </Typography>

            <TextField
              name="email"
              onChange={handleChange}
              id="standard-basic"
              label="Email"
              fullWidth
            />

            <div style={{ display: "flex", justifyContent: "flex-end" }}>
              <Button variant="contained" color="primary" onClick={generateOtp}>
                Send OTP
              </Button>
            </div>
          </form>
        </Paper>
      </div>
    </div>
  );
}

export default ForgotPassword;
