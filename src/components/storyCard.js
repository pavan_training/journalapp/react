import React, { useEffect } from "react";
import { Paper } from "@material-ui/core";
import renderHTML from "react-render-html";
import "./storyCard.css";

function StoryCard(props) {
  var tempDate = props.date;
  var time = getTimeAMPM(tempDate);
  var date = tempDate.getDate();
  var month = getMonthString(tempDate.getMonth());
  var year = tempDate.getFullYear();

  function getMonthString(num) {
    var months = {
      0: "Jan",
      1: "Feb",
      2: "Mar",
      3: "Apr",
      4: "May",
      5: "Jun",
      6: "Jul",
      7: "Aug",
      8: "Sep",
      9: "Oct",
      10: "Nov",
      11: "Dec",
    };

    return months[num];
  }

  function getTimeAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    return strTime;
  }

  return (
    <div style={{ marginBottom: 20 }}>
      <Paper
        style={{
          display: "flex",
          padding: "12px 0px",
          position: "relative",
        }}
      >
        <div
          className="left"
          style={{
            display: "flex",
            flexDirection: "column",
            textAlign: "center",
            width: 100,
          }}
        >
          <span style={{ fontSize: 32 }}>{date}</span>
          <span>
            {month}-{year}
          </span>
        </div>

        <div
          className="right module overflow"
          style={{ paddingRight: 12, flex: 1 }}
        >
          {renderHTML(props.description)}
        </div>

        <div style={{ position: "absolute", right: "12px", bottom: "6px" }}>
          {time}
        </div>
      </Paper>
    </div>
  );
}

export default StoryCard;
